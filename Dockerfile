# Pull base image 
From openjdk:8

# Maintainer 
MAINTAINER "sarartr2759@gmail.com" 
COPY ./webapp.war /usr/local/tomcat/webapps
EXPOSE 8085
ENTRYPOINT ["java","-jar","webapp.jar"]
